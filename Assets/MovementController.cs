﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController), typeof(Animator))]
public class MovementController : MonoBehaviour
{
    private float speed;
    private float gravity = -9.81f;

    public float movementSpeed;
    private Vector3 desiredMoveDirection;

    private Camera mainCamera;
    private CharacterController characterController;
    private Animator animator;

    [SerializeField]
    private float allowRotation = 0.1f;
    [SerializeField]
    private float rotationSpeed = 0.3f;

    [SerializeField]
    private float maxSpeed = 4f;
    [SerializeField]
    private float minSpeed = 1.5f;


    [SerializeField]
    private float gravityMultiplier;

    [SerializeField]
    private float speedSmoothTime = 0.1f;

    [SerializeField]
    private float speedDampTime = 0.1f;

    [SerializeField]
    private bool lockCursor = true;

    private Vector2 inputVector;

    private float targetAngle;

    private Quaternion startRotation;

    private int idleTurnRightHash;
    private int idleTurnLeftHash;
    private int idleTurnLeftTransHash;
    private int idleTurnRightTransHash;
    private int jumpHash;
    private int idleHash;
    private int idleToJumpHash;
    private int jumpToIdle;

    private AnimatorStateInfo currentAnimationStateInfo;
    private AnimatorTransitionInfo currentAnimatorTransitionInfo;

    private float velocityY = 0;

    [SerializeField]
    private float LocomotionThreshold = 0.2f;
    [SerializeField]
    private float jumpHeight = 1f;
    [SerializeField]
    private float jumpMultiplier = 1f;

    private bool HasInput { get { return inputVector != Vector2.zero; } }

    private bool jumpInput;
    private bool isTurning;
    private bool cursorLocked;
    private bool isJumping;


    private float prevYPos;
    private float jumpStartY;

    private Vector3 jumpVector;
    private bool TurnAnimIsPlaying { get => currentAnimationStateInfo.fullPathHash == idleTurnRightHash || currentAnimationStateInfo.fullPathHash == idleTurnLeftHash 
            || currentAnimatorTransitionInfo.fullPathHash == idleTurnLeftTransHash || currentAnimatorTransitionInfo.fullPathHash == idleTurnRightTransHash; }

    private bool isIdleAnimPlaying { get => currentAnimationStateInfo.fullPathHash == idleHash; }
    private bool isJumpAnimPlaying { get => currentAnimationStateInfo.fullPathHash == jumpHash || currentAnimatorTransitionInfo.fullPathHash == idleToJumpHash; }
    


    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

        idleTurnRightHash = Animator.StringToHash("Base Layer.Locomotion.TurnRightWalk");
        idleTurnLeftHash = Animator.StringToHash("Base Layer.Locomotion.TurnLeftWalk");
        idleHash = Animator.StringToHash("Base Layer.Locomotion.Idle");
        idleTurnLeftTransHash = Animator.StringToHash("Base Layer.Locomotion.Idle -> Base Layer.Locomotion.TurnLeftWalk");
        idleTurnRightTransHash = Animator.StringToHash("Base Layer.Locomotion.Idle -> Base Layer.Locomotion.TurnRightWalk");
        idleToJumpHash = Animator.StringToHash("Base Layer.Locomotion.Idle -> Base Layer.Jump");
        jumpHash = Animator.StringToHash("Base Layer.Jump");

        cursorLocked = lockCursor;
        HandleCursorLock();
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCursorLockState();

        currentAnimationStateInfo = animator.GetCurrentAnimatorStateInfo(0);
        currentAnimatorTransitionInfo = animator.GetAnimatorTransitionInfo(0);



        float inputX = Input.GetAxis("Horizontal");
        float inputZ = Input.GetAxis("Vertical");
        inputVector = new Vector2(inputX, inputZ);

        jumpInput = Input.GetButtonDown("Jump");

        speed = inputVector.magnitude;




        HandleMovement();

        UpdateAnimator();
       
    }


    private void HandleMovement()
    {
        if (characterController.isGrounded)
        {
            //velocityY =  -characterController.stepOffset / Time.deltaTime;
            velocityY = gravity * gravityMultiplier * Time.deltaTime;
        }
        else
        {
            //velocityY = -characterController.stepOffset / Time.deltaTime;

            velocityY += gravity * gravityMultiplier * Time.deltaTime;
        }

        if (isJumping && !isJumpAnimPlaying)
        {
            isJumping = false;
        }
        HandleRotation();

        if (jumpInput)
        {
            Jump();
        }
    }

    private void HandleRotation()
    {

        var forward = mainCamera.transform.forward;
        var right = mainCamera.transform.right;

        forward.y = 0;
        right.y = 0;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * inputVector.y + right * inputVector.x;
        desiredMoveDirection.Normalize();

        if (!TurnAnimIsPlaying)
        {
            targetAngle = 0;
            isTurning = false;
        }

        //We are playing the turning animation, rotate the transform, because no root motion is applied here
        if (TurnAnimIsPlaying)
        {
            var normalizedTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            var rotationProgress = Mathf.Lerp(0, 1f, normalizedTime);

            transform.rotation = Quaternion.AngleAxis(rotationProgress * targetAngle, Vector3.up) * startRotation;
        } else if (HasInput && Vector3.Angle(transform.forward, desiredMoveDirection) > 5.0f)
        {
            if (isIdleAnimPlaying)
            {
                startRotation = transform.rotation;
                targetAngle = Vector3.SignedAngle(transform.forward, desiredMoveDirection, transform.up);
                isTurning = true;
            } else
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed);
            }  
            
        }

    }

    private void OnAnimatorMove()
    {
        Vector3 moveDirection = animator.deltaPosition;
        if (isJumping)
        {
            //var y = animator.GetFloat("JumpCurve") * jumpMultiplier - (transform.position.y - jumpStartY);
            //prevYPos = animator.GetFloat("JumpCurve") * jumpMultiplier;
            //moveDirection = new Vector3(moveDirection.x, y, moveDirection.z);
            moveDirection = new Vector3(moveDirection.x, velocityY * Time.deltaTime, moveDirection.z) + jumpVector;
        }
        else
        {
            moveDirection = new Vector3(moveDirection.x, velocityY * Time.deltaTime, moveDirection.z);
        }


        characterController.Move(moveDirection);
        
    }


    private void UpdateAnimator()
    {
        animator.SetFloat("angle", targetAngle);
        animator.SetBool("isTurning", isTurning);
        animator.SetFloat("speed", speed, 0.1f, Time.deltaTime);
        animator.SetFloat("jump", characterController.velocity.y);
        animator.SetBool("isGrounded", characterController.isGrounded);
        animator.SetBool("isJumping", isJumping);
    }


    private void Jump()
    {
        if (characterController.isGrounded)
        {
            float jumpVelocity = Mathf.Sqrt(-2 * gravity * jumpHeight);
            velocityY = jumpVelocity;
            prevYPos = 0;
            jumpStartY = transform.position.y;
            jumpVector = animator.deltaPosition;
            isJumping = true;
        }
    }
    private void HandleCursorLock()
    {
        Cursor.lockState = cursorLocked ? CursorLockMode.Locked : CursorLockMode.None;
    }

    private void UpdateCursorLockState()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            cursorLocked = !cursorLocked;
            HandleCursorLock();
        }
    }
}
