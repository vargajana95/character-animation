# TPS character controller
A Third Person Shooter character controller made with Unity.

![](Images/image4.gif)

![](Images/image5.gif)

| | |
:-------------------------:|:-------------------------:
![](Images/image6.gif) | ![](Images/image7.gif)
![](Images/image8.gif) | ![](Images/image9.gif)

<img src="Images/image13.gif" width="400">

<img src="Images/image11.png" width="400">